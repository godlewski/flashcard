package com.lgi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.EditText;

import com.lgi.flashcards.R;

public class Utils {

	private static OnClickListener emptyClickListener = new OnClickListener() {

		public void onClick(DialogInterface dialog, int which) {
		}
	};
	private static Runnable progressDialogLauncher;
	protected static ProgressDialog progressDialog;

	public static interface OnOkClickListener {
		public void onOkClick(String inputValue);
	}

	public static void log(String str) {
		Log.d("LGI", str);
	}

	public static byte[] serialize(Serializable obj) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out;
			out = new ObjectOutputStream(bos);
			out.writeObject(obj);
			byte[] result = bos.toByteArray();
			out.close();
			bos.close();
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] serialize(Bitmap bitmap) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();
		try {
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmapdata;
	}

	public static Bitmap jpegFromBytes(byte[] data) {
		return BitmapFactory.decodeByteArray(data, 0, data.length);
	}

	public static Object deserialize(byte[] data) {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(data);
			ObjectInput in = new ObjectInputStream(bis);
			Object result = in.readObject();

			bis.close();
			in.close();
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void showInputAlert(Context context,
			final OnOkClickListener onOkClicked, String defaultValue) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		Resources res = context.getResources();
		alert.setTitle(res
				.getString(R.string.edit_flashcard_add_category_title));
		alert.setMessage(res.getString(R.string.edit_flashcard_enter_name));

		final EditText input = new EditText(context);
		if (defaultValue != null) {
			input.setText(defaultValue);
		}
		alert.setView(input);

		alert.setPositiveButton(res.getString(R.string.ok),
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						onOkClicked.onOkClick((input.getText().toString()));
					}
				}).setNegativeButton(res.getString(R.string.cancel),
				emptyClickListener);

		alert.show();
	}

	public static void showConfirmDialog(Context context, String title,
			String msg, OnClickListener onYesClicked) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		Resources res = context.getResources();
		alert.setTitle(title);
		alert.setMessage(msg);

		alert.setPositiveButton(res.getString(R.string.yes), onYesClicked)
				.setNegativeButton(res.getString(R.string.no),
						emptyClickListener);

		alert.show();
	}

	public static void showInputAlert(Context context,
			final OnOkClickListener onOkClicked) {
		showInputAlert(context, onOkClicked, null);
	}

	public synchronized static void showProgressDialog(final Activity activity,
			final String message) {
		hideProgressDialog();
		progressDialogLauncher = new Runnable() {
			public void run() {
				progressDialog = ProgressDialog.show(activity, "", message,
						true);
			}
		};
		activity.runOnUiThread(progressDialogLauncher);
	}

	public synchronized static void hideProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public static Date parseDate(String str) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			return convertDate(format.parse(str));
		} catch (ParseException e) {
			format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return convertDate(format.parse(str));
			} catch (ParseException e1) {
				return Utils.currentDate();
			}
		}
	}

	public static String dateToString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(date);
	}

	public static Date currentDate() {
		return convertDate(Calendar.getInstance().getTime());
	}

	private static Date convertDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
}
