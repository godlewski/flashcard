package com.lgi.flashcards.logic;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class State {
	private static final String SHARED_FILE = "LgiFlashcardsPreferences";

	private static final String KEY_CURRENT_CATEGORY = "selected_category";
	private static final String KEY_SHOW_MODE = "show_mode";

	public enum ShowMode {
		FIRST_PAGE, SECOND_PAGE, RANDOM
	}

	private static State instance;

	private Context context;
	private long selectedCategory = -1;
	private int baskets = 5;
	private ShowMode showMode = ShowMode.FIRST_PAGE;

	private State(Context context) {
		this.context = context;
	}

	public static void init(Context context) {
		if (instance == null) {
			instance = new State(context);
		} else {
			instance.context = context;
		}
		instance.load();
	}

	public static State getInstance() {
		return instance;
	}

	public long getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(long selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public ShowMode getShowMode() {
		return showMode;
	}

	public void setShowMode(ShowMode showMode) {
		this.showMode = showMode;
	}

	public int getBaskets() {
		return baskets;
	}

	public void setBaskets(int baskets) {
		this.baskets = baskets;
	}

	public void save() {
		SharedPreferences prefs = context.getSharedPreferences(SHARED_FILE,
				Context.MODE_PRIVATE);
		Editor edit = prefs.edit();
		edit.putLong(KEY_CURRENT_CATEGORY, selectedCategory);
		edit.putString(KEY_SHOW_MODE, showMode.name());
		edit.commit();
	}

	private void load() {
		SharedPreferences prefs = context.getSharedPreferences(SHARED_FILE,
				Context.MODE_PRIVATE);
		selectedCategory = prefs.getLong(KEY_CURRENT_CATEGORY, -1);
		showMode = ShowMode.valueOf(prefs.getString(KEY_SHOW_MODE,
				ShowMode.FIRST_PAGE.name()));
	}

}
