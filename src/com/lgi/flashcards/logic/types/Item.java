package com.lgi.flashcards.logic.types;

import com.lgi.flashcards.data.types.Flashcard;

public class Item {
	public enum Type {
		FLASHCARD, NO_NEW_FLASHCARDS, NEW_FLASHCARDS, NO_ANY_FLASHCARDS, FLASHCARDS
	}

	private Type type;
	private Flashcard flashcard;
	private int count;

	private Item() {

	}

	public static Item createFlashcard(Flashcard flashcard) {
		Item item = new Item();
		item.flashcard = flashcard;
		item.type = Type.FLASHCARD;
		return item;
	}

	public static Item createNoNewFlashcards() {
		Item item = new Item();
		item.type = Type.NO_NEW_FLASHCARDS;
		return item;
	}

	public static Item createNewFlashcards(int count) {
		Item item = new Item();
		item.type = Type.NEW_FLASHCARDS;
		item.count = count;
		return item;
	}

	public static Item createNoAnyFlashcards() {
		Item item = new Item();
		item.type = Type.NO_ANY_FLASHCARDS;
		return item;
	}

	public static Item createFlashcards(int count) {
		Item item = new Item();
		item.type = Type.FLASHCARDS;
		item.count = count;
		return item;
	}

	public Type getType() {
		return type;
	}

	public Flashcard getFlashcard() {
		return flashcard;
	}

	public int getCount() {
		return count;
	}
}
