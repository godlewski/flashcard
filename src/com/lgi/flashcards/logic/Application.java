package com.lgi.flashcards.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.database.Cursor;

import com.lgi.Utils;
import com.lgi.flashcards.Config;
import com.lgi.flashcards.data.DbAdapter;
import com.lgi.flashcards.data.DbApi;
import com.lgi.flashcards.data.DbModification;
import com.lgi.flashcards.data.ModificationDateComparator;
import com.lgi.flashcards.data.types.Basket;
import com.lgi.flashcards.data.types.Category;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.ModificationData;
import com.lgi.flashcards.data.types.PreparedListData;

public class Application {
	private static Application instance;

	private DbApi db;
	private int size;
	private List<Flashcard> preparedList;
	private List<Flashcard> bufferList;
	private Map<Long, String> categories;
	private List<ModificationData> modificationsList;
	private Map<Long, Basket> baskets;
	private Date nextCheckingDate;
	private DbGrabber grabber;
	private ApplicationListener listener;
	private int uncheckeds;
	private boolean needRestart;

	public static void init(Context context, ApplicationListener listener) {
		instance = new Application(context, listener);
	}

	public static Application getInstance() {
		return instance;
	}

	private Application(Context context, ApplicationListener listener) {
		this.needRestart = false;
		this.listener = listener;
		db = new DbAdapter(context);
		baskets = db.getBaskets();
		modificationsList = new ArrayList<ModificationData>();
		start();
	}

	private void start() {
		size = db.getSize();
		categories = db.getSelectedCategoriesNames();
		PreparedListData preparedListData = db
				.getOrderedFlashcards(new ArrayList<Long>());
		listener.uncheckedFlashcardCount(uncheckeds = preparedListData
				.getUncheckedSize());
		preparedList = preparedListData.getPreparedList();
		nextCheckingDate = preparedListData.getNextCheckingDate();
		bufferList = new ArrayList<Flashcard>();
		recalculateLocalFlashcards();
		if (size >= Config.BUFFER_SIZE) {
			grabber = new DbGrabber();
			grabber.start();
		}
		needRestart = false;
	}

	private void stop() {
		if (grabber != null) {
			grabber.interrupt();
		}
		grabber = null;
		List<Flashcard> flashcards = new ArrayList<Flashcard>();
		synchronized (this) {
			flashcards.addAll(preparedList);
			flashcards.addAll(bufferList);
			preparedList.clear();
			bufferList.clear();
		}
	}

	public Flashcard nextFlashcard() {
		if (size == 0) {
			return null;
		}

		if (size <= Config.BUFFER_SIZE) {
			if (preparedList.isEmpty()) {
				recalculateLocalFlashcards();
			}
			return pollPreparedFlashcard();
		}

		synchronized (this) {
			if (!preparedList.isEmpty()) {
				if (preparedList.size() < Config.BUFFER_MIN_SIZE) {
					grabber.grab();
				}
				return pollPreparedFlashcard();
			} else {
				try {
					listener.applicationStartedHardWorking();
					wait();
					listener.applicationFinishedHardWorking();
				} catch (InterruptedException e) {
					e.printStackTrace();
					return null;
				}
				return pollPreparedFlashcard();
			}
		}
	}

	public synchronized boolean isFlashcardInSelectedCategory(
			Flashcard flashcard) {
		return categories.containsKey(flashcard.getCategoryId());
	}

	private synchronized void recalculateLocalFlashcards() {
		preparedList.addAll(bufferList);
		Collections.sort(preparedList, new ModificationDateComparator(baskets));
		List<Flashcard> shuffled = new ArrayList<Flashcard>();
		Date currentDate = Utils.currentDate();
		for (ListIterator<Flashcard> i = preparedList.listIterator(); i
				.hasNext();) {
			Flashcard f = i.next();

			Date date = f.getModificationDate();
			long modificationMillis = date.getTime();
			Integer basketNr = f.getBasketNr();
			long periodMillis = baskets.get((long) basketNr).getPeriod() * 3600000;
			long currentMillis = currentDate.getTime();
			if (modificationMillis + periodMillis > currentMillis) {
				i.remove();
				shuffled.add(f);
			}
		}
		if (!shuffled.isEmpty()) {
			Collections.shuffle(shuffled);
			preparedList.addAll(shuffled);
		}
		bufferList.clear();
	}

	private synchronized Flashcard pollPreparedFlashcard() {
		Flashcard flashcard = preparedList.remove(0);
		flashcard.setModificationDate(Utils.currentDate());
		if (--uncheckeds == 0) {
			listener.uncheckedFlashcardCount(0);
		}
		bufferList.add(flashcard);
		return flashcard;
	}

	public long addCategory(String name) {
		long id = db.addCategory(name);
		if (id != -1) {
			categories.put(id, name);
		}
		return id;
	}

	public void add(Flashcard flashcard) {
		db.addFlashcard(flashcard);
		if (db.getCategory(flashcard.getCategoryId()).isSelected()) {
			size++;
			if (size == Config.BUFFER_SIZE) {
				grabber = new DbGrabber();
				grabber.start();
			} else if (size < Config.BUFFER_SIZE) {
				bufferList.add(flashcard);
			}
		}
	}

	public void close() {
		stop();
		db.close();
	}

	public void changeModificationDateAndBasket(Flashcard flashcard) {
		synchronized (modificationsList) {
			modificationsList.add(DbModification
					.createModificationDateModification(flashcard));
			modificationsList.add(DbModification
					.createChangeBasketNrModification(flashcard));
		}
	}

	public void changePages(Flashcard flashcard) {
		synchronized (modificationsList) {
			modificationsList.addAll(DbModification
					.createChangePagesModifications(flashcard));
		}
	}

	public Flashcard getFlashcard(long flashcardId) {
		synchronized (this) {
			for (Flashcard flashcard : bufferList) {
				if (flashcard.getId() == flashcardId) {
					return flashcard;
				}
			}
			for (Flashcard flashcard : preparedList) {
				if (flashcard.getId() == flashcardId) {
					return flashcard;
				}
			}
		}
		return db.getFlashcard(flashcardId);
	}

	public Category getCategory(long categoryId) {
		return db.getCategory(categoryId);
	}

	public Cursor getCategoriesCursor() {
		return db.getCategoriesCursor();
	}

	public void deleteCategory(long categoryId) {
		Category category = db.getCategory(categoryId);
		db.deleteCategory(categoryId);
		categories.remove(categoryId);
		if (category.isSelected() && category.getSize() > 0) {
			restart();
		}
	}

	public void restart() {
		stop();
		start();
	}

	public void renameCategory(long categoryId, String name) {
		categories.put(categoryId, name);
		db.renameCategory(categoryId, name);
	}

	public void setSelectedCategories(boolean selected) {
		db.setselectedCategories(selected);
		needRestart = true;
	}

	public void setSelectedCategory(long categoryId, boolean selected) {
		Category category = db.getCategory(categoryId);
		db.setSelectedCategory(categoryId, selected);
		if (category.isSelected() != selected && category.getSize() > 0) {
			needRestart = true;
		}
	}

	public String getCategoryName(long categoryId) {
		return categories.get(categoryId);
	}

	public interface ApplicationListener {
		void applicationStartedHardWorking();

		void applicationFinishedHardWorking();

		void uncheckedFlashcardCount(int count);
	}

	private class DbGrabber extends Thread {
		private boolean grabbing;

		public synchronized void grab() {
			if (!grabbing) {
				grabbing = true;
				notify();
			}
		}

		private void doGrab() {
			Application app = Application.this;
			long currentTime = System.currentTimeMillis();
			if (nextCheckingDate != null
					&& nextCheckingDate.getTime() < currentTime) {
				nextCheckingDate = null;
			}
			Set<Long> ids = new HashSet<Long>();
			synchronized (app) {
				for (Flashcard flashcard : preparedList) {
					ids.add(flashcard.getId());
				}
				for (Flashcard flashcard : bufferList) {
					ids.add(flashcard.getId());
				}
			}
			updateAll();
			PreparedListData listData = db.getOrderedFlashcards(ids);
			List<Flashcard> flashcards = listData.getPreparedList();
			nextCheckingDate = listData.getNextCheckingDate();
			synchronized (app) {
				preparedList.addAll(flashcards);
				recalculateLocalFlashcards();
				while (preparedList.size() > Config.BUFFER_SIZE) {
					preparedList.remove(preparedList.size() - 1);
				}
				app.notify();
			}
		}

		private void updateAll() {
			ArrayList<ModificationData> list = null;
			synchronized (modificationsList) {
				if (!modificationsList.isEmpty()) {
					list = new ArrayList<ModificationData>(modificationsList);
					modificationsList.clear();
				}
			}
			if (list != null) {
				db.executeFlashcardModifications(list);
			}
		}

		@Override
		public void run() {
			while (!isInterrupted()) {
				synchronized (this) {
					try {
						if (!grabbing) {
							waitToNextCheckingDate();
						}
					} catch (InterruptedException e) {
						break;
					}
				}
				doGrab();
				synchronized (this) {
					grabbing = false;
				}
			} // while
			updateAll();
		}

		private void waitToNextCheckingDate() throws InterruptedException {
			if (nextCheckingDate == null) {
				wait();
			} else {
				long current = System.currentTimeMillis();
				if (nextCheckingDate.getTime() > current) {
					wait(nextCheckingDate.getTime() - current);
				}
			}
		}
	}
	
	public boolean isNeedRestart() {
		return needRestart;
	}
}
