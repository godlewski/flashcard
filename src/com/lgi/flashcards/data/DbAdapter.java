package com.lgi.flashcards.data;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;

import com.lgi.Utils;
import com.lgi.flashcards.Config;
import com.lgi.flashcards.data.types.Basket;
import com.lgi.flashcards.data.types.Category;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.ModificationData;
import com.lgi.flashcards.data.types.Page;
import com.lgi.flashcards.data.types.PreparedListData;

public class DbAdapter implements DbApi {

	private DbOpenHelper helper;
	private SQLiteDatabase db;

	public DbAdapter(Context context) {
		helper = new DbOpenHelper(context);
		db = helper.getWritableDatabase();
	}

	public synchronized long addCategory(String name) {
		ContentValues values = new ContentValues();
		values.put(Category.NAME, name);
		return db.insert(Category.TABLE_NAME, null, values);
	}

	public synchronized void renameCategory(long categoryId, String name) {
		ContentValues values = new ContentValues();
		values.put(Category.NAME, name);
		db.update(Category.TABLE_NAME, values, Category.ID + "=" + categoryId,
				null);
	}

	public synchronized void setSelectedCategory(long categoryId,
			boolean selected) {
		ContentValues values = new ContentValues();
		values.put(Category.SELECTED, selected ? 1 : 0);
		db.update(Category.TABLE_NAME, values, Category.ID + "=" + categoryId,
				null);
	}

	public synchronized void setselectedCategories(boolean selected) {
		ContentValues values = new ContentValues();
		values.put(Category.SELECTED, selected ? 1 : 0);
		db.update(Category.TABLE_NAME, values, null, null);
	}

	public synchronized Cursor getCategoriesCursor() {
		Cursor cursor = db.query(Category.TABLE_NAME, new String[] {
				Category.ID, Category.NAME, Category.SIZE, Category.SELECTED },
				null, null, null, null, null);
		return cursor;
	}

	public synchronized Map<Long, String> getSelectedCategoriesNames() {
		Map<Long, String> result = new HashMap<Long, String>();
		Cursor c = db.query(Category.TABLE_NAME, new String[] { Category.ID,
				Category.NAME }, Category.SELECTED + "='1'", null, null, null,
				null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			long id = c.getLong(0);
			String name = c.getString(1);
			result.put(id, name);
			c.moveToNext();
		}
		c.close();
		return result;
	}

	public synchronized Category getCategory(long categoryId) {
		Cursor cursor = db.query(Category.TABLE_NAME, new String[] {
				Category.NAME, Category.SIZE, Category.SELECTED }, Category.ID
				+ "=" + categoryId, null, null, null, null);
		cursor.moveToFirst();
		Category category = cursor.getCount() == 0 ? null : new Category(
				categoryId, cursor.getString(0), cursor.getInt(2) == 1,
				cursor.getInt(1));
		cursor.close();
		return category;
	}

	public synchronized void deleteCategory(long categoryId) {
		db.beginTransaction();
		db.delete(Flashcard.TABLE_NAME, Category.ID + "=" + categoryId, null);
		db.delete(Category.TABLE_NAME, Category.ID + "=" + categoryId, null);
		db.setTransactionSuccessful();
		db.endTransaction();

	}

	public synchronized int getSize() {
		String sql = String.format("select count(*) from %s as f "
				+ "join %s as c on f.%s=c.%s" + " where c.%s='1'",
				Flashcard.TABLE_NAME, Category.TABLE_NAME,
				Flashcard.CATEGORY_ID, Category.ID, Category.SELECTED);
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		int size = cursor.getInt(0);
		cursor.close();
		return size;
	}

	private int getUncheckedSize(Collection<Long> exclusivedIds) {
		String sql = String
				.format("select datetime(f.%s, '+' || b.%s || ' hours') as left_time "
						+ "from %s as f "
						+ "join %s as b on f.%s=b.%s "
						+ "join %s as c on f.%s=c.%s "
						+ "where f.%s not in(%s) and left_time < '%s' and c.%s='1'",
						Flashcard.MODIFICATION_DATE, Basket.PERIOD,
						Flashcard.TABLE_NAME, Basket.TABLE_NAME,
						Flashcard.BASKET_NR, Basket.ID, Category.TABLE_NAME,
						Flashcard.CATEGORY_ID, Category.ID, Flashcard.ID,
						listToString(exclusivedIds),
						Utils.dateToString(Utils.currentDate()),
						Category.SELECTED);
		Cursor cursor = db.rawQuery(sql, null);
		int size = cursor.getCount();
		cursor.close();
		return size;
	}

	public synchronized long addFlashcard(Flashcard flashcard) {
		String date = Utils.dateToString(Utils.currentDate());
		ContentValues values = new ContentValues();
		values.put(Flashcard.BASKET_NR, 1);
		values.put(Flashcard.CATEGORY_ID, flashcard.getCategoryId());
		values.put(Flashcard.FIRST_PAGE, flashcard.getFirstPage().getData());
		values.put(Flashcard.FIRST_TYPE, flashcard.getFirstPage().getType());
		values.put(Flashcard.SECOND_PAGE, flashcard.getSecondPage().getData());
		values.put(Flashcard.SECOND_TYPE, flashcard.getSecondPage().getType());
		values.put(Flashcard.CREATION_DATE, date);
		values.put(Flashcard.MODIFICATION_DATE, date);
		flashcard.setId(db.insert(Flashcard.TABLE_NAME, null, values));
		return flashcard.getId();
	}

	public synchronized Flashcard getFlashcard(long flashcardId) {
		Cursor cursor = db.query(Flashcard.TABLE_NAME, new String[] {
				Flashcard.ID, Flashcard.CATEGORY_ID, Flashcard.FIRST_PAGE,
				Flashcard.FIRST_TYPE, Flashcard.SECOND_PAGE,
				Flashcard.SECOND_TYPE, Flashcard.CREATION_DATE,
				Flashcard.MODIFICATION_DATE, Flashcard.BASKET_NR },
				Flashcard.ID + "=" + flashcardId, null, null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() == 0) {
			throw new IllegalArgumentException(
					"Such flashcard not exists in DB");
		}
		Flashcard flashcard = createFlashcardFromCursor(cursor);
		cursor.close();
		return flashcard;
	}

	public synchronized PreparedListData getOrderedFlashcards(
			Collection<Long> exclusivedIds) {
		exclusivedIds = new ArrayList<Long>(exclusivedIds);
		int size = getUncheckedSize(exclusivedIds);
		List<Flashcard> flashcards = new ArrayList<Flashcard>();
		Date nextCheckingDate = null;
		int count = 0;
		Cursor cursor = db
				.rawQuery(
						uncheckedFlashcardsSQL(exclusivedIds,
								Config.BUFFER_SIZE), null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Flashcard flashcard = createFlashcardFromCursor(cursor);
			flashcards.add(flashcard);
			exclusivedIds.add(flashcard.getId());
			count++;
			cursor.moveToNext();
		}
		cursor.close();
		if (count < Config.BUFFER_SIZE) {
			String sql = randomFlashcardsSQL(exclusivedIds, Config.BUFFER_SIZE
					- count);
			cursor = db.rawQuery(sql, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Flashcard flashcard = createFlashcardFromCursor(cursor);
				flashcards.add(flashcard);
				cursor.moveToNext();
			}
		}
		cursor.close();
		cursor = db.rawQuery(nextCheckingDateSQL(), null);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			nextCheckingDate = Utils.parseDate(cursor.getString(0));
		}
		cursor.close();
		return new PreparedListData(flashcards, nextCheckingDate, size);
	}

	public static Flashcard createFlashcardFromCursor(Cursor cursor) {
		int firstTypeIdx = cursor.getColumnIndex(Flashcard.FIRST_TYPE);
		int firstPageIdx = cursor.getColumnIndex(Flashcard.FIRST_PAGE);
		int firstType = cursor.getInt(firstTypeIdx);
		byte[] firstPage = cursor.getBlob(firstPageIdx);
		Page page1 = Page.create(firstType, firstPage);
		Page page2 = Page.create(
				cursor.getInt(cursor.getColumnIndex(Flashcard.SECOND_TYPE)),
				cursor.getBlob(cursor.getColumnIndex(Flashcard.SECOND_PAGE)));
		int basketNrIdx = cursor.getColumnIndex(Flashcard.BASKET_NR);
		int basketNr = cursor.getInt(basketNrIdx);
		Flashcard flashcard = new Flashcard(cursor.getLong(cursor
				.getColumnIndex(Flashcard.CATEGORY_ID)), page1, page2,
				basketNr, Utils.parseDate(cursor.getString(cursor
						.getColumnIndex(Flashcard.CREATION_DATE))),
				(Utils.parseDate(cursor.getString(cursor
						.getColumnIndex(Flashcard.MODIFICATION_DATE)))));
		flashcard.setId(cursor.getLong(cursor.getColumnIndex(Flashcard.ID)));
		return flashcard;
	}

	private static String listToString(Collection<Long> exclusivedIds) {
		if (exclusivedIds.isEmpty()) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		Iterator<Long> i = exclusivedIds.iterator();
		while (true) {
			buffer.append(String.format("'%d'", i.next()));
			if (i.hasNext()) {
				buffer.append(",");
			} else {
				return buffer.toString();
			}
		}
	}

	public synchronized Map<Long, Basket> getBaskets() {
		Cursor cursor = db.query(Basket.TABLE_NAME, new String[] { Basket.ID,
				Basket.PERIOD }, null, null, null, null, null);
		cursor.moveToFirst();
		Map<Long, Basket> result = new HashMap<Long, Basket>();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			int period = cursor.getInt(1);
			result.put(id, new Basket(id, period));
			cursor.moveToNext();
		}
		cursor.close();
		return result;
	}

	public synchronized void executeFlashcardModifications(
			List<ModificationData> modificationsList) {
		boolean opened = db.isOpen();
		if (!opened) {
			db = helper.getWritableDatabase();
		}
		db.beginTransaction();
		for (ModificationData data : modificationsList) {
			db.update(Flashcard.TABLE_NAME, data.getValues(), Flashcard.ID
					+ "=" + data.getFlashcardId(), null);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		if (!opened) {
			db.close();
		}
	}

	public synchronized void close() {
		db.close();
	}

	private static String uncheckedFlashcardsSQL(
			Collection<Long> exclusivedIds, int limit) {
		String sql = String
				.format("select datetime(f.%s, '+' || b.%s || ' hours') as left_time,"
						+ "f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,b.%s "
						+ "from %s as f "
						+ "join %s as b on f.%s=b.%s "
						+ "join %s as c on f.%s=c.%s "
						+ "where f.%s not in(%s) and left_time < '%s' and c.%s='1'"
						+ "order by left_time " + "limit %d",
						Flashcard.MODIFICATION_DATE, Basket.PERIOD,
						Flashcard.ID, Flashcard.FIRST_PAGE,
						Flashcard.FIRST_TYPE, Flashcard.SECOND_PAGE,
						Flashcard.SECOND_TYPE, Flashcard.CATEGORY_ID,
						Flashcard.MODIFICATION_DATE, Flashcard.CREATION_DATE,
						Flashcard.BASKET_NR, Basket.PERIOD,
						Flashcard.TABLE_NAME, Basket.TABLE_NAME,
						Flashcard.BASKET_NR, Basket.ID, Category.TABLE_NAME,
						Flashcard.CATEGORY_ID, Category.ID, Flashcard.ID,
						listToString(exclusivedIds),
						Utils.dateToString(Utils.currentDate()),
						Category.SELECTED, limit);
		return sql;
	}

	private static String randomFlashcardsSQL(Collection<Long> exclusivedIds,
			int limit) {
		String sql = String.format(
				"select f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s,f.%s "
						+ "from %s as f join %s as c on f.%s=c.%s "
						+ "where f.%s not in(%s) and c.%s='1' "
						+ "order by random() limit %d", Flashcard.ID,
				Flashcard.FIRST_PAGE, Flashcard.FIRST_TYPE,
				Flashcard.SECOND_PAGE, Flashcard.SECOND_TYPE,
				Flashcard.CATEGORY_ID, Flashcard.MODIFICATION_DATE,
				Flashcard.CREATION_DATE, Flashcard.BASKET_NR,
				Flashcard.TABLE_NAME, Category.TABLE_NAME,
				Flashcard.CATEGORY_ID, Category.ID, Flashcard.ID,
				listToString(exclusivedIds), Category.SELECTED, limit);
		return sql;
	}

	private static String nextCheckingDateSQL() {
		return String.format(
				"select datetime(f.%s, '+' || b.%s || ' hours') as left_time "
						+ "from %s as f " + "join %s as b on f.%s=b.%s "
						+ "where left_time > '%s' " + "order by left_time "
						+ "limit 1", Flashcard.MODIFICATION_DATE,
				Basket.PERIOD, Flashcard.TABLE_NAME, Basket.TABLE_NAME,
				Flashcard.BASKET_NR, Basket.ID,
				Utils.dateToString(Utils.currentDate()));
	}

	public void test2() throws IOException {
		String[] files = new String[] { "Albania.png", "Andorra.png",
				"Austria.png", "Belarus.png", "Belgium.png",
				"Bosnia and Herzegovina.png", "Bulgaria.png", "Croatia.png",
				"Cyprus.png", "Czech Republic.png", "Denmark.png",
				"Estonia.png", "Finland.png", "France.png", "Georgia.png",
				"Germany.png", "Greece.png", "Hungary.png", "Iceland.png",
				"Ireland.png", "Italy.png", "latvia.png", "Liechtenstein.png",
				"Lithuania.png", "Luxembourg.png", "Macedonia.png",
				"Malta.png", "Monaco.png", "Montenegro.png", "Nederland.png",
				"Norway.png", "Poland.png", "Portugal.png",
				"Republic of Moldova.png", "Romania.png", "Russia.png",
				"San Marino.png", "Serbia.png", "Slovakia.png", "Slovenia.png",
				"Spain.png", "Sweden.png", "Switzerland.png", "Turkey.png",
				"Ukraine.png", "United Kingdom.png", "Vatican.png" };
		String[] names = new String[] { "Albania", "Andora", "Austria",
				"Bia�oru�", "Belgia", "Bo�nia i Hercegowina", "Bu�garia",
				"Chorwacja", "Cypr", "Czechy", "Dania", "Estonia", "Finlandia",
				"Francja", "Gruzja", "Niemcy", "Grecja", "W�gry", "Islandia",
				"Irlandia", "W�ochy", "�otwa", "Liechtenstein", "Litwa",
				"Luksemburg", "Macedonia", "Malta", "Monaco", "Czarnog�ra",
				"Nederland", "Norwegia", "Polska", "Portugalia",
				"Republika Mo�dowy", "Rumunia", "Rosja", "San Marino",
				"Serbia", "S�owacja", "S�owenia", "Hiszpania", "Szwecja",
				"Szwajcaria", "Turcja", "Ukraina", "Zjednoczone Kr�lestwo",
				"Watykan" };
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			map.put(files[i], names[i]);
		}

		String flagsURL = "http://dl.dropbox.com/u/533849/flags.zip";
		URL url = new URL(flagsURL);
		ZipInputStream in = new ZipInputStream(url.openStream());
		long categoryId = addCategory("Flagi pa�stw europejskich");
		while (true) {
			ZipEntry entry = in.getNextEntry();
			if (entry == null) {
				break;
			}
			byte[] buf = new byte[(int) entry.getSize()];
			int c;
			int size = 0;
			while ((c = in.read()) != -1) {
				buf[size++] = (byte) c;
			}
			Page p1 = Page.create(BitmapFactory.decodeByteArray(buf, 0, size));
			Page p2 = Page.create(map.get(entry.getName()));
			Date date = Utils.currentDate();
			Flashcard f = new Flashcard(categoryId, p1, p2, 1, date, date);
			addFlashcard(f);
			in.closeEntry();
		}
		in.close();
	}

	public void test() {
		String[][] countries = new String[][] {
				new String[] { "Albania", "Tirana" },
				new String[] { "Andora", "Andora" },
				new String[] { "Austria", "Wiede�" },
				new String[] { "Belgia", "Bruksela " },
				new String[] { "Bia�oru�", "Mi�sk" },
				new String[] { "Bo�nia i Hercegowina", "Sarajewo" },
				new String[] { "Bu�garia", "Sofia" },
				new String[] { "Chorwacja", "Zagrzeb" },
				new String[] { "Dania", "Kopenhaga" },
				new String[] { "Estonia", "Tallin" },
				new String[] { "Finlandia", "Helsinki" },
				new String[] { "Francja", "Pary�" },
				new String[] { "Grecja", "Ateny" },
				new String[] { "Hiszpania", "Madryt" },
				new String[] { "Holandia", "Amsterdam" },
				new String[] { "Irlandia", "Dublin" },
				new String[] { "Islandia", "Reykiavik" },
				new String[] { "Serbia i Czarnog�ra", "Belgrad" },
				new String[] { "Lichtenstein", "Vaduz" },
				new String[] { "Litwa", "Wilno" },
				new String[] { "Luksemburg", "Luksemburg" },
				new String[] { "�otwa", "Ryga" },
				new String[] { "Macedonia", "Skopie" },
				new String[] { "Malta", "Valletta" },
				new String[] { "Mo�dawia", "Kiszyni�w" },
				new String[] { "Monako", "Monako" },
				new String[] { "Niemcy", "Berlin" },
				new String[] { "Norwegia", "Oslo" },
				new String[] { "Polska", "Warszawa" },
				new String[] { "Portugalia", "Lizbona" },
				new String[] { "Republika Czeska", "Praga" },
				new String[] { "Rosja", "Moskwa" },
				new String[] { "Rumunia", "Bukareszt" },
				new String[] { "San Marino", "San Marino" },
				new String[] { "S�owacja", "Bratys�awa" },
				new String[] { "S�owenia", "Lublana" },
				new String[] { "Szwajcaria", "Berno" },
				new String[] { "Szwecja", "Sztokholm" },
				new String[] { "Ukraina", "Kij�w" },
				new String[] { "Watykan", "Watykan" },
				new String[] { "W�gry", "Budapeszt" },
				new String[] { "Wielka Brytania", "Londyn" },
				new String[] { "W�ochy", "Rzym" } };

		Date date = Utils.currentDate();
		long categoryId = addCategory("Stolice pa�stw europejskich");
		for (String[] pair : countries) {
			Page page1 = Page.create(pair[0]);
			Page page2 = Page.create(pair[1]);
			Flashcard f = new Flashcard(categoryId, page1, page2, 1, date, date);
			addFlashcard(f);
		}
	}
}
