package com.lgi.flashcards.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import android.database.Cursor;

import com.lgi.flashcards.data.types.Basket;
import com.lgi.flashcards.data.types.Category;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.ModificationData;
import com.lgi.flashcards.data.types.PreparedListData;

/**
 * 
 * @author lgi
 */
public interface DbApi {
	long addCategory(String name);

	void deleteCategory(long categoryId);

	Category getCategory(long categoryId);

	Map<Long, String> getSelectedCategoriesNames();

	void renameCategory(long categoryId, String name);

	void setSelectedCategory(long categoryId, boolean selected);

	void setselectedCategories(boolean selected);

	Cursor getCategoriesCursor();

	long addFlashcard(Flashcard flashcard);

	Flashcard getFlashcard(long flashcardId);

	PreparedListData getOrderedFlashcards(Collection<Long> exclusivedIds);

	Map<Long, Basket> getBaskets();

	void executeFlashcardModifications(List<ModificationData> modifications);

	int getSize();

	void close();

}
