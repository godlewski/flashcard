package com.lgi.flashcards.data;

import java.util.Comparator;
import java.util.Map;

import com.lgi.flashcards.data.types.Basket;
import com.lgi.flashcards.data.types.Flashcard;

public class ModificationDateComparator implements Comparator<Flashcard> {

	private final Map<Long, Basket> baskets;

	public ModificationDateComparator(Map<Long, Basket> baskets) {
		this.baskets = baskets;
	}

	public int compare(Flashcard f1, Flashcard f2) {
		Basket b1 = baskets.get((long) f1.getBasketNr());
		Basket b2 = baskets.get((long) f2.getBasketNr());
		long t1 = f1.getModificationDate().getTime() + b1.getPeriod() * 3600000;
		long t2 = f2.getModificationDate().getTime() + b2.getPeriod() * 3600000;
		return (int) (t1 - t2);
	}

}
