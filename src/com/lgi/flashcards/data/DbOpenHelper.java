package com.lgi.flashcards.data;

import com.lgi.flashcards.data.types.Basket;
import com.lgi.flashcards.data.types.Category;
import com.lgi.flashcards.data.types.Flashcard;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {

	private static final int DB_VERSION = 18;
	private static final String DB_NAME = "lgi_flashcards";

	public DbOpenHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(Flashcard.CREATE);
		db.execSQL(Category.CREATE);
		db.execSQL(Basket.CREATE);
		for (String trigger : Category.TRIGGERS) {
			db.execSQL(trigger);
		}
		for (String trigger : Flashcard.TRIGGERS) {
			db.execSQL(trigger);
		}
		for (String insert : Basket.INIT_RECORDS) {
			db.execSQL(insert);
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Flashcard.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + Category.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + Basket.TABLE_NAME);

		onCreate(db);
	}

}
