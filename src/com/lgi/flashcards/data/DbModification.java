package com.lgi.flashcards.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;

import com.lgi.Utils;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.ModificationData;
import com.lgi.flashcards.data.types.Page;

public class DbModification {

	public static List<ModificationData> createChangePagesModifications(
			Flashcard flashcard) {
		ArrayList<ModificationData> modifications = new ArrayList<ModificationData>();
		Page page = flashcard.getFirstPage();
		ContentValues values = new ContentValues();
		values.put(Flashcard.FIRST_TYPE, page.getType());
		values.put(Flashcard.FIRST_PAGE, page.getData());
		modifications.add(new ModificationData(values, flashcard.getId()));
		page = flashcard.getSecondPage();
		values = new ContentValues();
		values.put(Flashcard.SECOND_TYPE, page.getType());
		values.put(Flashcard.SECOND_PAGE, page.getData());
		modifications.add(new ModificationData(values, flashcard.getId()));
		return modifications;
	}

	public static ModificationData createChangeBasketNrModification(
			Flashcard flashcard) {
		Integer basketNr = flashcard.getBasketNr();
		if (basketNr != null) {
			ContentValues values = new ContentValues();
			values.put(Flashcard.BASKET_NR, basketNr);
			return new ModificationData(values, flashcard.getId());
		}
		return null;
	}

	public static ModificationData createModificationDateModification(
			Flashcard flashcard) {
		Date date = flashcard.getModificationDate();
		if (date != null) {
			ContentValues values = new ContentValues();
			values.put(Flashcard.MODIFICATION_DATE, Utils.dateToString(date));
			return new ModificationData(values, flashcard.getId());
		}
		return null;
	}
}
