package com.lgi.flashcards.data.types;

import java.util.Date;
import java.util.List;

public class PreparedListData {
	private final List<Flashcard> preparedList;
	private final Date nextCheckingDate;
	private final int uncheckedSize;

	public PreparedListData(List<Flashcard> preparedList,
			Date nextCheckingDate, int uncheckedSize) {
		this.preparedList = preparedList;
		this.nextCheckingDate = nextCheckingDate;
		this.uncheckedSize = uncheckedSize;
	}

	public List<Flashcard> getPreparedList() {
		return preparedList;
	}

	public Date getNextCheckingDate() {
		return nextCheckingDate;
	}

	public int getUncheckedSize() {
		return uncheckedSize;
	}

}
