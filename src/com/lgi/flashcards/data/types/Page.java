package com.lgi.flashcards.data.types;

import java.util.Arrays;

import android.graphics.Bitmap;

import com.lgi.Utils;

public class Page {
	public static final int TYPE_TEXT = 1 << 0;
	public static final int TYPE_BITMAP = 1 << 1;

	private int type;
	private byte[] data;

	private Page() {

	}

	public static Page create(String text) {
		Page page = new Page();
		page.type = TYPE_TEXT;
		page.data = Utils.serialize(text);
		return page;
	}

	public static Page create(Bitmap bitmap) {
		Page page = new Page();
		page.type = TYPE_BITMAP;
		page.data = Utils.serialize(bitmap);
		return page;
	}

	public static Page create(String text, Bitmap bitmap) {
		Page page = new Page();
		page.type = TYPE_TEXT | TYPE_BITMAP;
		BitmapTextData data = new BitmapTextData(text, bitmap);
		page.data = Utils.serialize(data);
		return page;
	}

	public static Page create(int type, byte[] serialized) {
		if (type == TYPE_TEXT) {
			return create((String) Utils.deserialize(serialized));
		} else if(type == TYPE_BITMAP) {
			return create(Utils.jpegFromBytes(serialized));
		} else if(type == (TYPE_BITMAP | TYPE_TEXT)) {
			BitmapTextData data = (BitmapTextData) Utils.deserialize(serialized);
			return create(data.getText(), data.getBitmap());
		} else {
			return null;
		}
	}

	public int getType() {
		return type;
	}

	public byte[] getData() {
		return data;
	}

	public String getText() {
		return (String) Utils.deserialize(data);
	}

	public Bitmap getBitmap() {
		return Utils.jpegFromBytes(data);
	}

	public BitmapTextData getBitmapTextData() {
		return (BitmapTextData) Utils.deserialize(data);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Page other = (Page) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
}
