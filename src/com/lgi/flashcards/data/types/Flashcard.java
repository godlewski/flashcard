package com.lgi.flashcards.data.types;

import java.util.Date;

import com.lgi.Utils;
import com.lgi.flashcards.Config;

public class Flashcard {

	public static final String TABLE_NAME = "flashcard";
	public static final String ID = "_id";
	public static final String CATEGORY_ID = "category_id";
	public static final String FIRST_PAGE = "first_page";
	public static final String SECOND_PAGE = "second_page";
	public static final String FIRST_TYPE = "first_type";
	public static final String SECOND_TYPE = "second_type";
	public static final String BASKET_NR = "basket_nr";
	public static final String MODIFICATION_DATE = "modification_date";
	public static final String CREATION_DATE = "creation_date";
	public static final String CREATE = String.format("CREATE TABLE %s ("
			+ "%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
			+ "%s BLOB NOT NULL," + "%s BLOB NOT NULL,"
			+ "%s INTEGER NOT NULL DEFAULT (1),"
			+ "%s INTEGER NOT NULL DEFAULT (1)," + "%s INTEGER NOT NULL, "
			+ "%s DATE NOT NULL DEFAULT '1990-01-01', "
			+ "%s DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,"
			+ "%s INTEGER NOT NULL)", TABLE_NAME, ID, FIRST_PAGE, SECOND_PAGE,
			FIRST_TYPE, SECOND_TYPE, CATEGORY_ID, MODIFICATION_DATE,
			CREATION_DATE, BASKET_NR);
	public static final String[] TRIGGERS = new String[] {
			String.format(
					"CREATE TRIGGER after_add_flashcard"
							+ " AFTER INSERT ON %s"
							+ " BEGIN"
							+ " update %s set %s=(select %s from %s where %s=NEW.%s)+1 where %s=NEW.%s;"
							+ " END;", TABLE_NAME, Category.TABLE_NAME,
					Category.SIZE, Category.SIZE, Category.TABLE_NAME,
					Category.ID, CATEGORY_ID, Category.ID, CATEGORY_ID),
			String.format(
					"CREATE TRIGGER before_delete_flashcard"
							+ " BEFORE DELETE ON %s"
							+ " BEGIN"
							+ " update %s set %s=(select %s from %s where %s=OLD.%s)-1 where %s=OLD.%s;"
							+ " END;", TABLE_NAME, Category.TABLE_NAME,
					Category.SIZE, Category.SIZE, Category.TABLE_NAME,
					Category.ID, CATEGORY_ID, Category.ID, CATEGORY_ID),
			String.format(
					"CREATE TRIGGER after_update_flashcard "
							+ "AFTER UPDATE ON %s "
							+ "BEGIN "
							+ "update %s set %s=(select %s from %s where %s=NEW.%s)+1 where %s=NEW.%s; "
							+ "update %s set %s=(select %s from %s where %s=NEW.%s)-1 where %s=OLD.%s; "
							+ "END", TABLE_NAME, Category.TABLE_NAME,
					Category.SIZE, Category.SIZE, Category.TABLE_NAME,
					Category.ID, CATEGORY_ID, Category.ID, CATEGORY_ID,
					Category.TABLE_NAME, Category.SIZE, Category.SIZE,
					Category.TABLE_NAME, Category.ID, CATEGORY_ID, Category.ID,
					CATEGORY_ID) };
	private long id;
	private long categoryId;
	private Page firstPage;
	private Page secondPage;
	private int basketNr;
	private Date creationDate;
	private Date modificationDate;

	public Flashcard(long categoryId, Page firstPage, Page secondPage,
			int basketNr, Date creationDate, Date modificationDate) {
		this.id = 0L;
		this.creationDate = creationDate;
		this.categoryId = categoryId;
		this.firstPage = firstPage;
		this.secondPage = secondPage;
		this.basketNr = basketNr;
		this.modificationDate = modificationDate;
	}

	public int getBasketNr() {
		return basketNr;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public Page getFirstPage() {
		return firstPage;
	}

	public long getId() {
		return id;
	}

	public Page getSecondPage() {
		return secondPage;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setId(long id) {
		if (this.id != 0L) {
			throw new IllegalStateException("ID is already set");
		}
		this.id = id;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public void setFirstPage(Page firstPage) {
		this.firstPage = firstPage;
	}

	public void setSecondPage(Page secondPage) {
		this.secondPage = secondPage;
	}

	public void setBasketNr(int basketNr) {
		this.basketNr = basketNr;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public String toString() {
		return String.format("Flashcard: id=%d, modification=%s", id,
				Utils.dateToString(modificationDate));
	}

	public void changeBasket(boolean up) {
		if (up && basketNr < Config.BASKETS_SIZE) {
			basketNr++;
		} else if (!up && basketNr > 1) {
			basketNr--;
		}
	}

}
