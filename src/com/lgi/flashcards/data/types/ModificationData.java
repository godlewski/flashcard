package com.lgi.flashcards.data.types;

import android.content.ContentValues;

public class ModificationData {
	private ContentValues values;
	private long flashcardId;

	public ModificationData(ContentValues values, long flashcardId) {
		this.values = values;
		this.flashcardId = flashcardId;
	}

	public ContentValues getValues() {
		return values;
	}

	public long getFlashcardId() {
		return flashcardId;
	}

}
