package com.lgi.flashcards.data.types;

public class Category {
	public static final String TABLE_NAME = "category";
	public static final String ID = "_id";
	public static final String NAME = "name";
	public static final String SIZE = "size";
	public static final String SELECTED = "selected";

	public static final String CREATE = String.format(
			"CREATE TABLE %s ("
					+ "%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
					+ "%s INTEGER DEFAULT 0, %s INTEGER DEFAULT 1,"
					+ "%s TEXT UNIQUE)", TABLE_NAME, ID, SIZE, SELECTED, NAME);

	public static final String[] TRIGGERS = new String[] { String.format(
			"CREATE TRIGGER after_delete_category"
					+ " AFTER DELETE ON %s BEGIN "
					+ "delete from %s where %s=OLD.%s; END", TABLE_NAME,
			Flashcard.TABLE_NAME, Flashcard.CATEGORY_ID, ID) };

	private final long id;
	private final String name;
	private final boolean selected;
	private final int size;

	public Category(long id, String name, boolean selected, int size) {
		this.id = id;
		this.name = name;
		this.selected = selected;
		this.size = size;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isSelected() {
		return selected;
	}

	public int getSize() {
		return size;
	}

}
