package com.lgi.flashcards.data.types;

import java.io.Serializable;

import com.lgi.Utils;

import android.graphics.Bitmap;

public class BitmapTextData implements Serializable {
	private static final long serialVersionUID = 8932867601823072352L;

	private final String text;
	private final byte[] bitmapBytes;

	public BitmapTextData(String text, Bitmap bitmap) {
		this.text = text;
		this.bitmapBytes = Utils.serialize(bitmap);
	}

	public String getText() {
		return text;
	}

	public Bitmap getBitmap() {
		return Utils.jpegFromBytes(bitmapBytes);
	}

}
