package com.lgi.flashcards.data.types;

public class Basket {

	public static final String TABLE_NAME = "basket";
	public static final String ID = "_id";
	public static final String PERIOD = "period";
	
	public static final String CREATE = String.format("CREATE TABLE %s ("
			+ "%s INTEGER PRIMARY KEY NOT NULL, " + "%s INTEGER NOT NULL)",
			TABLE_NAME, ID, PERIOD);
	
	public static final String[] INIT_RECORDS = new String[] {
			String.format("insert into %s(%s, %s) values(%d, %d)", TABLE_NAME,
					ID, PERIOD, 1, 12),
			String.format("insert into %s(%s, %s) values(%d, %d)", TABLE_NAME,
					ID, PERIOD, 2, 24),
			String.format("insert into %s(%s, %s) values(%d, %d)", TABLE_NAME,
					ID, PERIOD, 3, 48),
			String.format("insert into %s(%s, %s) values(%d, %d)", TABLE_NAME,
					ID, PERIOD, 4, 72),
			String.format("insert into %s(%s, %s) values(%d, %d)", TABLE_NAME,
					ID, PERIOD, 5, 168) };

	private final long id;
	private final int period;

	public Basket(long id, int period) {
		this.id = id;
		this.period = period;
	}

	public long getId() {
		return id;
	}

	public int getPeriod() {
		return period;
	}

	@Override
	public String toString() {
		return "Basket [id=" + id + ", period=" + period + "]";
	}
}
