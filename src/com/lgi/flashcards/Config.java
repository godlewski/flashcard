package com.lgi.flashcards;

public class Config {
	public static final int IMG_WIDTH = 200;
	public static final int IMG_HEIGHT = 150;

	public static final int VERTICAL_VELOCITY = 2000;
	public static final int HORIZONTAL_VELOCITY = 700;

	public static final float VERTICAL_SENSITIVITY = 3;
	public static final int FLASHCARDS_QUEUE_MIN_SIZE = 20;
	public static final int BUFFER_SIZE = 10;
	public static final int BUFFER_MIN_SIZE = 5;
	public static final int BASKETS_SIZE = 5;
}
