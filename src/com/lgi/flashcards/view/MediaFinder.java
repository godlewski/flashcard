package com.lgi.flashcards.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.lgi.flashcards.Config;

import com.lgi.flashcards.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

public class MediaFinder {

	private static final int ACTION_CAMERA_REQUEST = 101;
	private static final int ACTION_PICTURE_FILE_REQUEST = 102;
	private static final String TMP_FILE_NAME = "last_pict.jpg";

	private final Activity activity;

	public MediaFinder(Activity activity) {
		this.activity = activity;
	}

	public void getBitmapFromGallery() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		activity.startActivityForResult(
				Intent.createChooser(
						intent,
						activity.getResources().getString(
								R.string.edit_flashcard_choose_app)),
				ACTION_PICTURE_FILE_REQUEST);
	}

	public void getBitmapFromCamera() {
		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()));
		activity.startActivityForResult(intent, ACTION_CAMERA_REQUEST);
	}

	private File getTempFile() {
		final File path = new File(Environment.getExternalStorageDirectory(),
				activity.getPackageName());
		if (!path.exists()) {
			path.mkdir();
		}
		return new File(path, TMP_FILE_NAME);
	}

	public Bitmap getBitmapResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return null;
		}

		Bitmap bitmap = null;
		try {
			switch (requestCode) {
			case ACTION_CAMERA_REQUEST:
				bitmap = getThumbnail(activity, Uri.fromFile(getTempFile()),
						Config.IMG_WIDTH, Config.IMG_HEIGHT);
				break;

			case ACTION_PICTURE_FILE_REQUEST:
				bitmap = getThumbnail(activity, data.getData(),
						Config.IMG_WIDTH, Config.IMG_HEIGHT);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	private Bitmap getThumbnail(Context context, Uri uri, int maxWidth,
			int maxHeight) throws FileNotFoundException, IOException {
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither = true;// optional
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
		BitmapFactory.decodeStream(input, null, options);
		input.close();
		if ((options.outWidth == -1) || (options.outHeight == -1))
			return null;

		int origWidth = options.outWidth;
		int origHeight = options.outHeight;

		double ratioWidth = (origWidth > maxWidth) ? (origWidth / maxWidth)
				: 1.0;
		double ratioHeight = (origHeight > maxHeight) ? (origHeight / maxHeight)
				: 1.0;
		double ratio = Math.max(ratioWidth, ratioHeight);

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither = true;// optional
		bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		input.close();
		return bitmap;
	}

	private int getPowerOfTwoForSampleRatio(double ratio) {
		int k = Integer.highestOneBit((int) Math.floor(ratio));
		if (k == 0)
			return 1;
		else
			return k;
	}

}
