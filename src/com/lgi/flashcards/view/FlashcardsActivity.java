package com.lgi.flashcards.view;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.lgi.Utils;
import com.lgi.flashcards.R;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.Page;
import com.lgi.flashcards.logic.Application;
import com.lgi.flashcards.logic.Application.ApplicationListener;
import com.lgi.flashcards.logic.State;
import com.lgi.flashcards.view.controls.BitmapTextPainter;
import com.lgi.flashcards.view.controls.TwoSideView;
import com.lgi.flashcards.view.controls.TwoSideView.TwoSideViewListener;

public class FlashcardsActivity extends Activity implements
		TwoSideViewListener, ApplicationListener {

	private static final int CODE_EDIT = 10;
	private static final int CODE_SELECT_CATEGORY = 11;
	private static Flashcard EMPTY_FLASHCARD = null;

	private TwoSideView twoSideView;
	private Application application;
	private Flashcard flashcard;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Application.init(this, this);
		application = Application.getInstance();
		if (EMPTY_FLASHCARD == null) {
			createEmptyFlashcard();
		}
		State.init(this);

		setContentView(R.layout.main);
		twoSideView = (TwoSideView) findViewById(R.id.main_flashcard_view);
		Resources resources = getResources();
		Bitmap bg = ((BitmapDrawable) resources
				.getDrawable(R.drawable.flashcard)).getBitmap();
		BitmapTextPainter painter = new BitmapTextPainter(bg);
		twoSideView.setPainter(painter);
		twoSideView.setListener(this);
		loadNextFlashcard();
		twoSideView.setFlashcard(flashcard);
		((Button) findViewById(R.id.main_button_good))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						twoSideView.hide();
						if (flashcard != null && flashcard != EMPTY_FLASHCARD) {
							flashcard.changeBasket(true);
						}
					}
				});

		((Button) findViewById(R.id.main_button_bad))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						twoSideView.hide();
						if (flashcard != null && flashcard != EMPTY_FLASHCARD) {
							flashcard.changeBasket(false);
						}
					}
				});
	}

	private void loadNextFlashcard() {
		flashcard = application.nextFlashcard();

		if (flashcard == null) {
			flashcard = EMPTY_FLASHCARD;
		}
	}

	private void createEmptyFlashcard() {
		Resources res = getResources();
		Flashcard flashcard = new Flashcard(-1, Page.create(res
				.getString(R.string.main_hello_screen)), Page.create(res
				.getString(R.string.main_no_flashcards)), 1,
				Utils.currentDate(), Utils.currentDate());
		EMPTY_FLASHCARD = flashcard;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.flashcards, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		State.getInstance().save();
		application.close();
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_flashcard:
			addFlashcard();
			return true;

		case R.id.edit_flashcard:
			editFlashcard();
			return true;

		case R.id.select_categories:
			selectCategories();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void addFlashcard() {
		Intent intent = new Intent()
				.setClass(this, EditFlashcardActivity.class);
		startActivity(intent);
	}

	private void editFlashcard() {
		if (flashcard == null) {
			Toast.makeText(this, R.string.main_no_flashcard_to_edit,
					Toast.LENGTH_SHORT).show();
		} else {
			Intent intent = new Intent().setClass(this,
					EditFlashcardActivity.class);
			Bundle extras = new Bundle();
			extras.putLong(EditFlashcardActivity.PARAM_FLASHCARD_ID,
					flashcard.getId());
			intent.putExtras(extras);
			startActivityForResult(intent, CODE_EDIT);
		}
	}

	private void selectCategories() {
		Intent intent = new Intent().setClass(this,
				SelectCategoriesActivity.class);
		startActivityForResult(intent, CODE_SELECT_CATEGORY);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data == null) {
			return;
		}
		if (requestCode == CODE_EDIT && resultCode == RESULT_OK) {
			twoSideView.setFlashcard(flashcard);
		} else if (requestCode == CODE_SELECT_CATEGORY
				&& resultCode == RESULT_OK) {
			if (flashcard == EMPTY_FLASHCARD
					|| (flashcard != EMPTY_FLASHCARD && !application
							.isFlashcardInSelectedCategory(flashcard))) {
				loadNextFlashcard();
				twoSideView.setFlashcard(flashcard);
			}
		}
	}

	public void onHideCard(TwoSideView component) {
		if (flashcard != null && flashcard != EMPTY_FLASHCARD) {
			application.changeModificationDateAndBasket(flashcard);
		}
		Flashcard flashcard = application.nextFlashcard();
		if (flashcard == null) {
			flashcard = EMPTY_FLASHCARD;
		}
		if (this.flashcard != flashcard) {
			this.flashcard = flashcard;
		}
		component.setFlashcard(flashcard);
	}

	public void startWaiting() {
		Utils.showProgressDialog(this, getString(R.string.please_wait));
	}

	public void endWaiting() {
		Utils.hideProgressDialog();
	}

	public void applicationStartedHardWorking() {
		startWaiting();
	}

	public void applicationFinishedHardWorking() {
		endWaiting();

	}

	public void uncheckedFlashcardCount(int count) {
		String msg = getString(R.string.no_flashcards_to_check_msg);
		if (count > 0) {
			msg = String.format(getString(R.string.flashcards_to_check_msg),
					count);
		}
		final String MSG = msg;
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(FlashcardsActivity.this, MSG, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Utils.showProgressDialog(this, getString(R.string.please_wait));
		new Thread() {
			public void run() {
				Application app = Application.getInstance();
				if (app.isNeedRestart()) {
					app.restart();
				}
				Utils.hideProgressDialog();
			}
		}.start();

	}
}