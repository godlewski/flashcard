package com.lgi.flashcards.view.controls;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;

import com.lgi.Utils;
import com.lgi.flashcards.data.types.BitmapTextData;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.Page;
import com.lgi.flashcards.logic.Application;
import com.lgi.flashcards.view.controls.TwoSideView.Painter;

public class BitmapTextPainter implements Painter {

	private static final TextPaint PAINT_HEADER;

	private final Bitmap background;
	private Bitmap firstBitmap;
	private Bitmap secondBitmap;
	boolean updatedBitmaps;
	private Flashcard flashcard;
	private StaticLayout categoryLayout;
	private int headerHeight;
	private int bodyHeight;

	public BitmapTextPainter(Bitmap background) {
		this.background = background;
		updatedBitmaps = false;
	}

	public void paintFirstSide(Canvas canvas, int width, int height) {
		updateBitmaps();
		canvas.drawBitmap(firstBitmap, 0, 0, null);
	}

	public void paintSecondSide(Canvas canvas, int width, int height) {
		updateBitmaps();
		canvas.drawBitmap(secondBitmap, 0, 0, null);
	}

	private synchronized void updateBitmaps() {
		if (!updatedBitmaps) {
			Flashcard flashcard = this.flashcard;
			int width = background.getWidth();
			int height = background.getHeight();
			String category = Application.getInstance().getCategoryName(
					flashcard.getCategoryId());
			if(category == null) {
				category = "brak kategorii";
			}
			category = String.format("%s\nKoszyk %d\nModyfikacja: %s",
					category, flashcard.getBasketNr(),
					Utils.dateToString(flashcard.getModificationDate()));
			String categoryName = category == null ? "" : category;
			categoryLayout = new StaticLayout(categoryName, PAINT_HEADER,
					width, Alignment.ALIGN_CENTER, 1, 0, true);
			headerHeight = categoryLayout.getHeight();
			bodyHeight = height - headerHeight;
			firstBitmap = background.copy(Config.ARGB_8888, true);

			Canvas canvas = new Canvas(firstBitmap);
			categoryLayout.draw(canvas);
			canvas.translate(0, headerHeight);
			createPage(flashcard.getFirstPage(), canvas, width);

			secondBitmap = background.copy(Config.ARGB_8888, true);
			canvas = new Canvas(secondBitmap);
			categoryLayout.draw(canvas);
			canvas.translate(0, headerHeight);
			createPage(flashcard.getSecondPage(), canvas, width);

			updatedBitmaps = true;
		}
	}

	private void createPage(Page page, Canvas canvas, int width) {
		int bodyHeight = this.bodyHeight;
		if (page.getType() == Page.TYPE_TEXT) {
			StaticLayout layout = createSideLayout(page.getText(), width,
					bodyHeight);
			drawTextOnPage(layout, canvas, bodyHeight);
		} else if (page.getType() == Page.TYPE_BITMAP) {
			Bitmap bitmap = page.getBitmap();
			drawBitmapOnPage(bitmap, canvas, width, bodyHeight);
		} else if (page.getType() == (Page.TYPE_BITMAP | Page.TYPE_TEXT)) {
			BitmapTextData data = page.getBitmapTextData();
			int bitmapHeight = (int) (bodyHeight * 0.7f);
			bitmapHeight = drawBitmapOnPage(data.getBitmap(), canvas, width,
					bitmapHeight);
			StaticLayout layout = createSideLayout(data.getText(), width,
					bodyHeight - bitmapHeight);
			canvas.translate(0, bitmapHeight);
			drawTextOnPage(layout, canvas, bodyHeight - bitmapHeight);
		}
	}

	private int drawBitmapOnPage(Bitmap bitmap, Canvas canvas, int maxWidth,
			int maxHeight) {
		int bmpWidth = bitmap.getWidth();
		int bmpHeight = bitmap.getHeight();
		float ratio = (float) bmpWidth / bmpHeight;
		int width = Math.min(maxWidth, (int) (ratio * maxHeight));
		int height = (int) (width / ratio);
		bmpWidth = Math.min(bmpWidth, width);
		bmpHeight = Math.min(bmpHeight, height);
		int x = (maxWidth - bmpWidth) / 2;
		int y = (maxHeight - bmpHeight) / 2;
		Rect rect = new Rect(x, y, x + bmpWidth, y + bmpHeight);
		canvas.drawBitmap(bitmap,
				new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), rect,
				null);
		return bmpHeight;
	}

	private void drawTextOnPage(StaticLayout layout, Canvas canvas,
			int bodyHeight) {
		int height = layout.getHeight();
		canvas.translate(0, (bodyHeight - height) / 2);
		layout.draw(canvas);
	}

	private StaticLayout createSideLayout(String text, int width, int height) {
		int fontSize = 40;
		StaticLayout layout = null;
		do {
			TextPaint paint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
			paint.setTextSize(fontSize);
			layout = new StaticLayout(text, paint, width,
					Alignment.ALIGN_CENTER, 1, 0, true);
			fontSize -= 2;
		} while (layout.getHeight() > height && fontSize > 5);
		return layout;
	}

	static {
		PAINT_HEADER = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		PAINT_HEADER.setTextSize(15);
	}

	public synchronized void setFlashcard(Flashcard flashcard) {
		this.flashcard = flashcard;
		this.updatedBitmaps = false;
	}

};