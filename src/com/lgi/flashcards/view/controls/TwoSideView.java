package com.lgi.flashcards.view.controls;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.view.controls.AnimationStateMachineThread.Action;
import com.lgi.flashcards.view.controls.AnimationStateMachineThread.ActionListener;

public class TwoSideView extends SurfaceView implements Callback,
		OnTouchListener, ActionListener {

	private static final int MARGIN_H = 10;
	private static final int MARGIN_V = 10;
	public static final int FIRST_PAGE = 0;
	public static final int SECOND_PAGE = 1;
	private static final float DIRECTION_THRESHOLD = 3;
	private static final float NEXT_SENSITIVE = 2f;

	private enum Direction {
		HORIZONTAL, VERTICAL, UNKNOWN
	}

	public interface TwoSideViewListener {
		public void onHideCard(TwoSideView component);
	}

	private int cardWidth;
	private int cardHeight;

	private Painter painter;

	private int angle;
	private int y;

	private AnimationStateMachineThread animationThread;

	private PointF startPosition;
	private float currentPositionX;
	private float currentPositionY;
	private Direction direction;
	private TwoSideViewListener listener;

	public TwoSideView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TwoSideView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TwoSideView(Context context) {
		super(context);
		init();
	}

	public void setListener(TwoSideViewListener listener) {
		this.listener = listener;
	}

	public void setPainter(Painter painter) {
		this.painter = painter;
	}

	private void init() {
		direction = Direction.UNKNOWN;
		currentPositionX = 0;
		angle = 0;
		setZOrderOnTop(true);
		SurfaceHolder holder = getHolder();
		holder.setFormat(PixelFormat.TRANSPARENT);
		setOnTouchListener(this);
		holder.addCallback(this);
		if (animationThread == null) {
			(animationThread = new AnimationStateMachineThread(this)).start();
		}
	}

	void draw() {
		if (painter == null) {
			return;
		}
		SurfaceHolder holder = getHolder();
		Canvas canvas = holder.lockCanvas();
		if (canvas == null) {
			return;
		}
		Camera camera = new Camera();
		camera.rotateY(angle);
		Matrix matrix = new Matrix();
		camera.getMatrix(matrix);
		matrix.preTranslate(-cardWidth / 2, y);
		matrix.postTranslate(cardWidth / 2 + MARGIN_H, MARGIN_V);
		canvas.drawColor(0, PorterDuff.Mode.CLEAR);
		canvas.setMatrix(matrix);
		if (isFirstPage()) {
			painter.paintFirstSide(canvas, cardWidth, cardHeight);
		} else {
			canvas.scale(-1, 1, cardWidth / 2, cardHeight / 2);
			painter.paintSecondSide(canvas, cardWidth, cardHeight);
		}

		holder.unlockCanvasAndPost(canvas);
	}

	public boolean isFirstPage() {
		return (angle + 90) % 360 < 180;
	}

	public boolean isSecondPage() {
		return !isFirstPage();
	}

	public void setPage(int nr) {
		if (nr == FIRST_PAGE) {
			angle = 0;
		} else if (nr == SECOND_PAGE) {
			angle = 180;
		} else {
			throw new IllegalArgumentException("Page nr = " + nr);
		}
		draw();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		cardWidth = width - 2 * MARGIN_H;
		cardHeight = height - 2 * MARGIN_V;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		init();
		cardWidth = getWidth() - 2 * MARGIN_H;
		cardHeight = getHeight() - 2 * MARGIN_V;
		draw();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		animationThread.setDestroy();
		animationThread = null;
	}

	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			startPosition = new PointF(event.getX(), event.getY());
			direction = Direction.UNKNOWN;
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			if (direction == Direction.UNKNOWN) {
				PointF start = startPosition;
				float dx = start.x - event.getX();
				float dy = start.y - event.getY();
				float length = PointF.length(dx, dy);
				if (length > DIRECTION_THRESHOLD) {
					if (Math.abs(dy) > NEXT_SENSITIVE * Math.abs(dx)) {
						direction = Direction.VERTICAL;
						currentPositionY = start.y;
					} else {
						direction = Direction.HORIZONTAL;
						currentPositionX = start.x;
					}
				}
			}
			if (direction == Direction.HORIZONTAL) {
				angle = (int) ((angle + (event.getX() - currentPositionX) * .7f) % 360);
				while (angle < 0) {
					angle += 360;
				}
				currentPositionX = event.getX();
			} else if (direction == Direction.VERTICAL) {
				y += event.getY() - currentPositionY;
				currentPositionY = event.getY();
			}
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			animationThread.startAction(Action.Back);
		}

		draw();
		return true;
	}

	public interface Painter {
		public void paintFirstSide(Canvas canvas, int width, int height);

		public void paintSecondSide(Canvas canvas, int width, int height);

		public void setFlashcard(Flashcard flashcard);
	}

	public void setFlashcard(Flashcard flashcard) {
		painter.setFlashcard(flashcard);
		if (animationThread != null) {
			animationThread.startAction(Action.Show);
		}
	}

	public int getHeigth() {
		return cardHeight;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void actionDone(Action action) {
		draw();
		if (action == Action.Hide) {
			if (listener != null) {
				setPage(0);
				listener.onHideCard(this);
			} else {
				animationThread.startAction(Action.Show);
			}
		}
	}

	public void refresh() {
		draw();
	}

	public void hide() {
		animationThread.startAction(Action.Hide);
	}

}
