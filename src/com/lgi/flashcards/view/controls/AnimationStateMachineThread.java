package com.lgi.flashcards.view.controls;

import com.lgi.flashcards.Config;

public class AnimationStateMachineThread extends Thread {

	public static enum Action {
		Rotate, Hide, Show, Back
	}

	public static interface ActionListener {
		public int getHeigth();

		public void setAngle(int a);

		public void setY(int y);

		public int getAngle();

		public int getY();

		public void actionDone(Action action);

		public void refresh();
	}

	private long currentTime;
	private Action action;
	private final ActionListener listener;
	private boolean destroyed;

	public AnimationStateMachineThread(ActionListener listener) {
		this.listener = listener;
		destroyed = false;
	}

	public synchronized void startAction(Action action) {
		this.action = action;
		this.notify();
	}

	@Override
	public void run() {
		while (!destroyed) {
			if (currentTime == 0) {
				currentTime = System.currentTimeMillis();
			}
			Action action = this.action;
			if (action != null) {

				long time = System.currentTimeMillis();
				int dt = (int) (time - currentTime);
				switch (action) {

				case Rotate:
					rotate(dt);
					break;

				case Hide:
					hide(dt);
					break;

				case Show:
					show(dt);
					break;

				case Back:
					back(dt);
					break;

				}

				listener.refresh();
				try {
					sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				currentTime = 0;
				try {
					synchronized (this) {
						wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void show(int dt) {
		ActionListener listener = this.listener;
		boolean toUp = listener.getY() < 0;
		int V = Config.VERTICAL_VELOCITY;
		int dy = dt * V / 1000;
		int newY = listener.getY() + (toUp ? dy : -dy);
		if ((toUp && newY >= 0) || (!toUp && newY <= 0)) {
			listener.setY(0);
			done(Action.Show);
		} else {
			listener.setY(newY);
		}
	}

	private void hide(int dt) {
		ActionListener listener = this.listener;
		boolean toUp = listener.getY() >= 0;
		int V = Config.VERTICAL_VELOCITY;
		int dy = dt * V / 1000;
		int newY = listener.getY() + (toUp ? dy : -dy);
		int heigth = listener.getHeigth() + 50;
		if (toUp && newY >= heigth) {
			listener.setY(-heigth);
			done(Action.Hide);
		} else if (!toUp && newY <= -heigth) {
			listener.setY(heigth);
			done(Action.Hide);
		} else {
			listener.setY(newY);
		}
	}

	private void back(int dt) {
		ActionListener listener = this.listener;
		if (listener.getAngle() % 180 != 0) {
			action = Action.Rotate;
		} else if (listener.getY() == 0) {
			action = null;
		} else if (Math.abs(listener.getY()) > listener.getHeigth()
				/ Config.VERTICAL_SENSITIVITY) {
			action = Action.Hide;
		} else {
			action = Action.Show;
		}
	}

	private void rotate(int dt) {
		int da = Config.HORIZONTAL_VELOCITY * dt / 1000;
		int angle = listener.getAngle();
		if (angle < 90) {
			int newAngle = angle - da;
			if (newAngle < 0) {
				newAngle = 0;
			}
			angle = newAngle;
			if (angle == 0) {
				done(Action.Rotate);
			}
		} else if (angle < 180) {
			int newAngle = angle + da;
			if (newAngle > 180) {
				newAngle = 180;
			}
			angle = newAngle;
			if (angle == 180) {
				done(Action.Rotate);
			}
		} else if (angle < 270) {
			int newAngle = angle - da;
			if (newAngle < 180) {
				newAngle = 180;
			}
			angle = newAngle;
			if (angle == 180) {
				done(Action.Rotate);
			}
		} else {
			int newAngle = angle + da;
			if (newAngle >= 360) {
				newAngle = 0;
			}
			angle = newAngle;
			if (angle == 0) {
				done(Action.Rotate);
			}
		}
		listener.setAngle(angle);
	}

	private synchronized void done(Action action) {
		currentTime = 0;
		this.action = null;
		listener.actionDone(action);
	}

	public synchronized void setDestroy() {
		this.destroyed = true;
		notify();
	}

}
