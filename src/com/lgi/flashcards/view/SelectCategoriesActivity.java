package com.lgi.flashcards.view;

import java.util.HashMap;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.lgi.Utils;
import com.lgi.Utils.OnOkClickListener;
import com.lgi.flashcards.R;
import com.lgi.flashcards.data.types.Category;
import com.lgi.flashcards.logic.Application;
import com.lgi.flashcards.logic.State;
import com.lgi.flashcards.view.adapters.SelectCategoriesAdapter;
import com.lgi.flashcards.view.adapters.SelectCategoriesAdapter.OnCheckedRecordChangeListener;

public class SelectCategoriesActivity extends ListActivity implements
		OnCheckedRecordChangeListener {
	public static final String PARAM_SELECTION_CHANGED = "selection_changed";
	private Long selectedCategoryId;
	private String selectedCategoryName;
	private HashMap<Long, Pair<Boolean, Boolean>> selectedCategories;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSelectedCategories();
		fillCategories();
	}

	private void initSelectedCategories() {
		HashMap<Long, Pair<Boolean, Boolean>> categories = new HashMap<Long, Pair<Boolean, Boolean>>();
		Cursor cursor = Application.getInstance().getCategoriesCursor();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			boolean isSelected = (cursor.getInt(3) == 1);
			categories.put(cursor.getLong(0), new Pair<Boolean, Boolean>(
					isSelected, isSelected));
			cursor.moveToNext();
		}
		cursor.close();
		selectedCategories = categories;
	}

	private void fillCategories() {
		Cursor categories = Application.getInstance().getCategoriesCursor();
		startManagingCursor(categories);
		CursorAdapter adapter = new SelectCategoriesAdapter(this, categories,
				this);
		setListAdapter(adapter);
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.select_categories, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.add_category:
			addCategory();
			return true;

		case R.id.select_all:
			selectAll();
			return true;

		case R.id.unselect_all:
			unselectAll();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void addCategory() {
		Utils.showInputAlert(this, new OnOkClickListener() {
			public void onOkClick(String input) {
				long selectedCategory = Application.getInstance().addCategory(
						input);
				State.getInstance().setSelectedCategory(selectedCategory);
				fillCategories();
				selectedCategories.put(selectedCategory,
						new Pair<Boolean, Boolean>(false, true));
			}
		});
	}

	private void selectAll() {
		Application.getInstance().setSelectedCategories(true);
		fillCategories();
		refreshAfterSelectionAll(true);
	}

	private void unselectAll() {
		Application.getInstance().setSelectedCategories(false);
		fillCategories();
		refreshAfterSelectionAll(false);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.category_row) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.edit_category, menu);
			selectedCategoryId = (Long) ((SelectCategoriesAdapter.Fields) v
					.getTag()).selected.getTag();
			selectedCategoryName = ((SelectCategoriesAdapter.Fields) v.getTag()).name
					.getText().toString();
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.edit_category_rename:
			renameCategory();
			return true;
		case R.id.edit_category_remove:
			removeCategory();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	private void renameCategory() {
		Utils.showInputAlert(this, new OnOkClickListener() {
			public void onOkClick(String inputValue) {
				Application.getInstance().renameCategory(selectedCategoryId,
						inputValue);
				fillCategories();
			}
		}, selectedCategoryName);
	}

	private void removeCategory() {
		Category category = Application.getInstance().getCategory(
				selectedCategoryId);
		if (category.getSize() > 0) {
			Resources res = getResources();
			Utils.showConfirmDialog(
					this,
					res.getString(R.string.edit_flashcard_remove_category_title),
					res.getString(R.string.edit_flashcard_remove_category_confirm),
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							doRemoveCategory();
						}

					});
		} else {
			doRemoveCategory();
		}
	}

	private void doRemoveCategory() {
		Utils.showProgressDialog(this, getString(R.string.please_wait));
		new Thread() {
			public void run() {
				Application.getInstance().deleteCategory(selectedCategoryId);
				Pair<Boolean, Boolean> pair = selectedCategories
						.get(selectedCategoryId);
				selectedCategories.put(selectedCategoryId, new Pair<Boolean, Boolean>(
						pair.first, false));
				runOnUiThread(new Runnable() {
					public void run() {
						fillCategories();
						Utils.hideProgressDialog();
					}
				});
			}
		}.start();
	}

	private void refreshAfterSelectionAll(boolean selected) {
		Cursor cursor = Application.getInstance().getCategoriesCursor();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			Pair<Boolean, Boolean> pair = selectedCategories.get(id);
			selectedCategories.put(id, new Pair<Boolean, Boolean>(pair.first,
					selected));
			cursor.moveToNext();
		}
		cursor.close();
	}

	public void onCheckedChange(long id, boolean isChecked) {
		Application.getInstance().setSelectedCategory(id, isChecked);
		HashMap<Long, Pair<Boolean, Boolean>> categories = selectedCategories;
		Pair<Boolean, Boolean> pair = categories.get(id);
		categories.put(id, new Pair<Boolean, Boolean>(pair.first, isChecked));
	}

	@Override
	public void onBackPressed() {
		boolean selectionChanged = false;
		for (Pair<Boolean, Boolean> selectionPair : selectedCategories.values()) {
			if (selectionPair.first != selectionPair.second) {
				selectionChanged = true;
				break;
			}
		}
		sendResultAndFinish(selectionChanged);
	}

	private void sendResultAndFinish(boolean selectionChanged) {
		Intent data = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean(PARAM_SELECTION_CHANGED, selectionChanged);
		data.putExtras(bundle);
		setResult(RESULT_OK, data);
		finish();
	}
}
