package com.lgi.flashcards.view;

import java.util.Date;

import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.Toast;

import com.lgi.Utils;
import com.lgi.Utils.OnOkClickListener;
import com.lgi.flashcards.R;
import com.lgi.flashcards.data.types.BitmapTextData;
import com.lgi.flashcards.data.types.Flashcard;
import com.lgi.flashcards.data.types.Page;
import com.lgi.flashcards.logic.Application;
import com.lgi.flashcards.logic.State;
import com.lgi.flashcards.view.adapters.CategoriesAdapter;

public class EditFlashcardActivity extends TabActivity {
	public static final String PARAM_FLASHCARD_ID = "flashcard_id";

	private static final String TAG_FIRST_PAGE = "first";
	private static final String TAG_SECOND_PAGE = "second";
	private static final String TAG_CATEGORY = "category";

	private static final String SAVED_CURRENT_TAB = "current_tab";
	private static final String SAVED_BITMAP1 = "bitmap1";
	private static final String SAVED_BITMAP2 = "bitmap2";

	private static long selectedCategory = -1;
	private static String selectedCategoryName = "";

	private MediaFinder mediaFinder;
	private ImageView imageView1;
	private ImageView imageView2;
	private EditText editText1;
	private EditText editText2;
	private ListView categoriesList;
	private Bitmap bitmap1;
	private Bitmap bitmap2;
	private int currentTab;
	private long flashcardId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_flashcard);
		flashcardId = -1;
		mediaFinder = new MediaFinder(this);
		initFields();
		fillCategories();
		restoreState(savedInstanceState);
		initTabs(currentTab);
		getTabHost().setOnTabChangedListener(new OnTabChangeListener() {
			public void onTabChanged(String tabId) {
				currentTab = tabId.equals(TAG_FIRST_PAGE) ? 0 : tabId
						.equals(TAG_SECOND_PAGE) ? 1 : 2;
			}
		});

		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey(PARAM_FLASHCARD_ID)) {
			flashcardId = extras.getLong(PARAM_FLASHCARD_ID);
			fillFlashcard(Application.getInstance().getFlashcard(flashcardId));
		}
	}

	private void initFields() {
		imageView1 = (ImageView) findViewById(R.id.picture1);
		imageView2 = (ImageView) findViewById(R.id.picture2);
		editText1 = (EditText) findViewById(R.id.text1);
		editText2 = (EditText) findViewById(R.id.text2);
		categoriesList = (ListView) findViewById(R.id.categories_list);
	}

	private void restoreState(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		currentTab = 0;
		if (savedInstanceState.containsKey(SAVED_CURRENT_TAB)) {
			currentTab = savedInstanceState.getInt(SAVED_CURRENT_TAB);
		}
		if (savedInstanceState.containsKey(SAVED_BITMAP1)) {
			setBitmap1((Bitmap) savedInstanceState.getParcelable(SAVED_BITMAP1));
		}
		if (savedInstanceState.containsKey(SAVED_BITMAP2)) {
			setBitmap2((Bitmap) savedInstanceState.getParcelable(SAVED_BITMAP2));
		}
	}

	private void fillFlashcard(Flashcard flashcard) {
		Page page1 = flashcard.getFirstPage();
		if (page1.getType() == Page.TYPE_TEXT) {
			editText1.setText(page1.getText());
		} else if (page1.getType() == Page.TYPE_BITMAP) {
			setBitmap1(page1.getBitmap());
		} else if (page1.getType() == (Page.TYPE_BITMAP | Page.TYPE_TEXT)) {
			BitmapTextData data = page1.getBitmapTextData();
			editText1.setText(data.getText());
			setBitmap1(data.getBitmap());
		} else {
			throw new IllegalArgumentException("wrong type of flashcard's page");
		}
		Page page2 = flashcard.getSecondPage();
		if (page2.getType() == Page.TYPE_TEXT) {
			editText2.setText(page2.getText());
		} else if (page2.getType() == Page.TYPE_BITMAP) {
			setBitmap2(page2.getBitmap());
		} else if (page2.getType() == (Page.TYPE_BITMAP | Page.TYPE_TEXT)) {
			BitmapTextData data = page2.getBitmapTextData();
			editText2.setText(data.getText());
			setBitmap2(data.getBitmap());
		} else {
			throw new IllegalArgumentException("wrong type of flashcard's page");
		}

		if (flashcard.getCategoryId() != State.getInstance()
				.getSelectedCategory()) {
			State.getInstance().setSelectedCategory(flashcard.getCategoryId());
			fillCategories();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(SAVED_CURRENT_TAB, getTabHost().getCurrentTab());
		if (bitmap1 != null) {
			outState.putParcelable(SAVED_BITMAP1, bitmap1);
		}
		if (bitmap2 != null) {
			outState.putParcelable(SAVED_BITMAP2, bitmap2);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.edit_flashcard_page, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean result = super.onPrepareOptionsMenu(menu);

		boolean isShowingCategories = currentTab == 2;
		menu.setGroupVisible(R.id.group_page, !isShowingCategories);
		menu.findItem(R.id.add_category).setVisible(isShowingCategories);

		if (!isShowingCategories) {
			MenuItem removeItem = menu.findItem(R.id.remove_picture);
			boolean removePictureVisible = currentTab == 0 && bitmap1 != null
					|| currentTab == 1 && bitmap2 != null;
			removeItem.setVisible(removePictureVisible);
		}
		return result;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.set_picture_camera:
			mediaFinder.getBitmapFromCamera();
			return true;

		case R.id.set_picture_file:
			mediaFinder.getBitmapFromGallery();
			return true;

		case R.id.remove_picture:
			freeBitmap();
			return true;

		case R.id.add_category:
			addCategory();
			return true;

		case R.id.save_flashcard:
			saveFlashcard();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.category_row) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.edit_category, menu);
			selectedCategory = (Long) ((CategoriesAdapter.Fields) v.getTag()).radio
					.getTag();
			selectedCategoryName = ((CategoriesAdapter.Fields) v.getTag()).name
					.getText().toString();
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.edit_category_rename:
			renameCategory();
			return true;
		case R.id.edit_category_remove:
			removeCategory();
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	private void saveFlashcard() {
		int errorMsg = -1;
		String text1 = editText1.getText().toString().trim();
		String text2 = editText2.getText().toString().trim();
		if (text1.length() == 0 && bitmap1 == null) {
			errorMsg = R.string.edit_flashcard_empty_page_1;
		} else if (text2.length() == 0 && bitmap2 == null) {
			errorMsg = R.string.edit_flashcard_empty_page_2;
		} else if (State.getInstance().getSelectedCategory() == -1) {
			errorMsg = R.string.edit_flashcard_no_category_selected;
		}

		if (errorMsg != -1) {
			Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
			return;
		}

		Page page1 = createPage(text1, bitmap1);
		Page page2 = createPage(text2, bitmap2);
		if (flashcardId != -1) {
			Flashcard flashcard = Application.getInstance().getFlashcard(
					flashcardId);
			flashcard.setFirstPage(page1);
			flashcard.setSecondPage(page2);
			Application.getInstance().changePages(flashcard);
		} else {
			Date date = Utils.currentDate();
			Flashcard flashcard = new Flashcard(State.getInstance()
					.getSelectedCategory(), page1, page2, 1, date, date);
			Application.getInstance().add(flashcard);
		}
		Intent data = new Intent();
		if (flashcardId != -1) {
			Bundle bundle = new Bundle();
			bundle.putLong(PARAM_FLASHCARD_ID, flashcardId);
			data.putExtras(bundle);
		}
		setResult(RESULT_OK, data);
		finish();
	}

	private static Page createPage(String text, Bitmap bitmap) {
		Page page;
		if (bitmap == null) {
			page = Page.create(text);
		} else if (text.length() == 0) {
			page = Page.create(bitmap);
		} else {
			page = Page.create(text, bitmap);
		}
		return page;
	}

	private void removeCategory() {
		Resources res = getResources();
		Utils.showConfirmDialog(this,
				res.getString(R.string.edit_flashcard_remove_category_title),
				res.getString(R.string.edit_flashcard_remove_category_confirm),
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Application app = Application.getInstance();
						app.deleteCategory(selectedCategory);
						app.restart();
						fillCategories();
					}
				});
	}

	private void renameCategory() {
		Utils.showInputAlert(this, new OnOkClickListener() {
			public void onOkClick(String inputValue) {
				Application.getInstance().renameCategory(selectedCategory,
						inputValue);
				fillCategories();
			}
		}, selectedCategoryName);
	}

	private void addCategory() {
		Utils.showInputAlert(this, new OnOkClickListener() {
			public void onOkClick(String input) {
				State.getInstance().setSelectedCategory(
						Application.getInstance().addCategory(input));
				fillCategories();
			}
		});
	}

	private void freeBitmap() {
		if (currentTab == 0 && bitmap1 != null) {
			imageView1.setImageBitmap(null);
			bitmap1.recycle();
			bitmap1 = null;
		} else if (currentTab == 1 && bitmap2 != null) {
			imageView2.setImageBitmap(null);
			bitmap2.recycle();
			bitmap2 = null;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Bitmap bitmap = mediaFinder.getBitmapResult(requestCode, resultCode,
				data);
		if (bitmap != null) {
			boolean page1 = currentTab == 0;
			if (page1) {
				setBitmap1(bitmap);
			} else {
				setBitmap2(bitmap);
			}
		}
	}

	private void fillCategories() {
		Cursor cursor = Application.getInstance().getCategoriesCursor();
		startManagingCursor(cursor);
		CursorAdapter adapter = new CategoriesAdapter(this, cursor);
		categoriesList.setLongClickable(true);
		categoriesList.setAdapter(adapter);
		categoriesList.setOnCreateContextMenuListener(this);
		registerForContextMenu(categoriesList);
	}

	private void initTabs(int currentTab) {
		Resources res = getResources();
		TabHost tabHost = getTabHost();
		TabHost.TabSpec spec;
		spec = tabHost
				.newTabSpec(TAG_FIRST_PAGE)
				.setIndicator(
						res.getString(R.string.edit_flashcard_first_page),
						res.getDrawable(R.drawable.ic_tab_edit_one))
				.setContent(R.id.page1);
		tabHost.addTab(spec);

		spec = tabHost
				.newTabSpec(TAG_SECOND_PAGE)
				.setIndicator(
						res.getString(R.string.edit_flashcard_second_page),
						res.getDrawable(R.drawable.ic_tab_edit_two))
				.setContent(R.id.page2);
		tabHost.addTab(spec);
		tabHost.setCurrentTab(currentTab);

		spec = tabHost
				.newTabSpec(TAG_CATEGORY)
				.setIndicator(res.getString(R.string.edit_flashcard_category),
						res.getDrawable(R.drawable.ic_tab_edit_categories))
				.setContent(R.id.categories_list);
		tabHost.addTab(spec);
		tabHost.setCurrentTab(currentTab);
	}

	private void setBitmap1(Bitmap bitmap) {
		imageView1.setVisibility(ImageView.VISIBLE);
		imageView1.setImageBitmap(bitmap);
		bitmap1 = bitmap;
	}

	private void setBitmap2(Bitmap bitmap) {
		imageView2.setVisibility(ImageView.VISIBLE);
		imageView2.setImageBitmap(bitmap);
		bitmap2 = bitmap;
	}

}
