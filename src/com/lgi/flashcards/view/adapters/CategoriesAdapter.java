package com.lgi.flashcards.view.adapters;

import java.util.HashSet;

import com.lgi.flashcards.R;
import com.lgi.flashcards.logic.State;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CursorAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

public class CategoriesAdapter extends CursorAdapter implements
		OnCheckedChangeListener {

	public static class Fields {
		public TextView name;
		public TextView size;
		public RadioButton radio;

		Fields(TextView name, TextView size, RadioButton radio) {
			this.name = name;
			this.size = size;
			this.radio = radio;
		}

	}

	HashSet<RadioButton> radios;

	public CategoriesAdapter(Context context, Cursor c) {
		super(context, c);
		radios = new HashSet<RadioButton>();
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			Long id = (Long) buttonView.getTag();
			State.getInstance().setSelectedCategory(id);
			for (RadioButton radioButton : radios) {
				Long categoryId = (Long) radioButton.getTag();
				radioButton.setChecked(categoryId == State.getInstance()
						.getSelectedCategory());
			}
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.category_row, parent, false);
		((Activity) context).registerForContextMenu(v);
		Fields fields = new Fields((TextView) v.findViewById(R.id.name),
				(TextView) v.findViewById(R.id.size),
				(RadioButton) v.findViewById(R.id.select));
		final long id = cursor.getLong(0);
		fields.name.setText(cursor.getString(1));
		fields.size.setText(cursor.getString(2));
		fields.radio
				.setChecked(State.getInstance().getSelectedCategory() == id);
		fields.radio.setTag(id);
		fields.radio.setOnCheckedChangeListener(this);
		v.setTag(fields);
		radios.add(fields.radio);
		return v;
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {
		long id = c.getLong(0);
		Fields fields = (Fields) v.getTag();
		fields.name.setText(c.getString(1));
		fields.size.setText(c.getString(2));
		fields.radio.setTag(id);
		fields.radio
				.setChecked(id == State.getInstance().getSelectedCategory());
	}

}
