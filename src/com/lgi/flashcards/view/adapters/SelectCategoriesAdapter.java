package com.lgi.flashcards.view.adapters;

import com.lgi.flashcards.R;
import com.lgi.flashcards.logic.State;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class SelectCategoriesAdapter extends CursorAdapter implements
		OnCheckedChangeListener {

	private final OnCheckedRecordChangeListener listener;

	public class Fields {

		public Fields(TextView name, TextView size, CheckBox selected) {
			this.name = name;
			this.size = size;
			this.selected = selected;
		}

		public TextView name;
		public TextView size;
		public CheckBox selected;
	}

	public interface OnCheckedRecordChangeListener {
		public void onCheckedChange(long id, boolean isChecked);
	}

	public SelectCategoriesAdapter(Context context, Cursor c,
			OnCheckedRecordChangeListener listener) {
		super(context, c);
		this.listener = listener;
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			Long id = (Long) buttonView.getTag();
			State.getInstance().setSelectedCategory(id);
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.categories_row, parent, false);
		((Activity) context).registerForContextMenu(v);
		final long id = cursor.getLong(0);
		CheckBox checkBox = (CheckBox) v.findViewById(R.id.select);
		TextView nameView = (TextView) v.findViewById(R.id.name);
		TextView sizeView = (TextView) v.findViewById(R.id.size);
		nameView.setText(cursor.getString(1));
		sizeView.setText(cursor.getString(2));
		checkBox.setChecked(cursor.getInt(3) == 1);
		Fields fields = new Fields(nameView, sizeView, checkBox);
		checkBox.setTag(id);
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				long id = (Long) buttonView.getTag();
				listener.onCheckedChange(id, isChecked);
			}
		});
		v.setTag(fields);
		return v;
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {
		Fields fields = (Fields) v.getTag();
		CheckBox checkBox = fields.selected;
		long id = c.getLong(0);
		checkBox.setTag(id);
		checkBox.setChecked(c.getInt(3) == 1);
		fields.name.setText(c.getString(1));
		fields.size.setText(c.getString(2));
	}

}
